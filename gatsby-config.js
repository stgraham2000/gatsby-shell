module.exports = {
  plugins: [
    'gatsby-theme-material-ui',
    'gatsby-plugin-typescript'
  ],
  siteMetadata: {
    title: 'My page',
  },
};
