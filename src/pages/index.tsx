import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import ProTip from '../components/ProTip';
import TestDrawer from '../components/TestDrawer'
import TestAppBar from '../components/TestAppBar';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: "100px"
  }
}));

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

export default function Index() {
  const classes = useStyles();

  return (
    <div>
      <TestAppBar />
      <Container className={classes.container} maxWidth="sm">
        <Box my={4}>
          <Typography variant="h4" component="h1" gutterBottom>
            Gatsby v4-beta example
          </Typography>
          <Link href="/about" color="secondary">
            Go to about page
          </Link>
          <ProTip />
          <Copyright />
          <TestDrawer />
        </Box>
      </Container>
    </div>
  );
}
