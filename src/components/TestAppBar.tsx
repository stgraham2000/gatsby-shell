import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Select from '@material-ui/core/Select';
import Link from '@material-ui/core/Link';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import IconButton from '@material-ui/core/IconButton';
import ExpandMore from '@material-ui/icons/ExpandMore';


const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  bar: {
    minHeight: "50px"
  },
  iconButton: {
    fontSize: "14px"
  },
  link: {
    marginLeft: "8px",
    marginRight: "8px"
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  logo: {
    width: "100px",
    marginLeft: "50px",
    marginTop: "10px",
    marginBottom: "10px",
    transition: "0.3s width ease"
  },
  logoCollapsed: {
    width: "50px"
  }
}));

function CollapsibleLogo(props: any) {
  const classes = useStyles();

  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
    target: window ? window() : undefined,
  });

  return (
    <img className={trigger? [classes.logo, classes.logoCollapsed].join(" ") : classes.logo} src="/logo.png"></img>
  )
}

export default function TestAppBar(props: any) {
  const classes = useStyles();
  const [auth, setAuth] = React.useState(true);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleChange = (event: any) => {
    setAuth(event.target.checked);
  };

  const handleMenu = (event: any) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className={classes.root}>
      <AppBar position="fixed">
        <Toolbar className={classes.bar}>
          <div className={classes.grow}>
            <CollapsibleLogo />
          </div>
          <Link className={classes.link} color="textSecondary" href="/">Product & Technology</Link>
          <div>
            <IconButton
              className={classes.iconButton}
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleMenu}
              color="inherit"
            >
              Company
              <ExpandMore />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={open}
              onClose={handleClose}
            >
              <MenuItem onClick={handleClose}>Profile</MenuItem>
              <MenuItem onClick={handleClose}>My account</MenuItem>
            </Menu>
          </div>
          <Link className={classes.link} color="textSecondary" href="/">Consulting</Link>
          <Link className={classes.link} color="textSecondary" href="/">Press Room</Link>
          <Link className={classes.link} color="textSecondary" href="/">Contact</Link>
        </Toolbar>
      </AppBar>
    </div>
  )
}